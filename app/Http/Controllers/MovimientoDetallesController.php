<?php

namespace App\Http\Controllers;

use App\User;
use App\Producto;
use App\Movimiento;
use App\MovimientoDetalles;
use Illuminate\Http\Request;

class MovimientoDetallesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos=Producto::all()->pluck('id');
        $movimientos=Movimiento::all()->pluck('id');
        return view('movimientosDetalles.index')->with(compact('movimientos', 'productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MovimientoDetalles  $movimientoDetalles
     * @return \Illuminate\Http\Response
     */
    public function show(MovimientoDetalles $movimientoDetalles)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MovimientoDetalles  $movimientoDetalles
     * @return \Illuminate\Http\Response
     */
    public function edit(MovimientoDetalles $movimientoDetalles)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MovimientoDetalles  $movimientoDetalles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MovimientoDetalles $movimientoDetalles)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MovimientoDetalles  $movimientoDetalles
     * @return \Illuminate\Http\Response
     */
    public function destroy(MovimientoDetalles $movimientoDetalles)
    {
        //
    }
}
