<?php

namespace App\Http\Controllers;

use App\Movimiento;
use App\Producto;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
class ProductoController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $movimientos=Movimiento::all()->pluck('folio');
        $productos=Producto::all()->sortByDesc('id');
        return view('productos.index') ->with(compact('productos','movimientos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $movimientos=Movimiento::all()->pluck('folio');
        $movimientos->last();
        // dd($movimientos->first());
        
        $productos=Producto::all()->sortByDesc('id');
        $productos->first();
        return view('productos.create')->with(compact('productos', 'movimientos'));
        // return view('productos.create', $data);
        // return view('productos.create')->with('');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $movimientos=Movimiento::all();
        $productos=Producto::all();   
        $productos= request()->except('_token');
        $request->validate([
            'codigo'=>['required'],
            'descripcion'=>['required'],
            'precio'=>['required'],
            'unidad'=>['required'],
            'status'=>['required'],
        ]);                
        Producto::insert($productos);
        return redirect()->route('productos.create'); //la cambie
        }

    /**
     * Display the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function prueba(){

        $productos=Producto::all();
        dd($productos);

    return view('');   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movimientos=Movimiento::all();
        $productos=Producto::all();
        $productos=Producto::findOrFail($id);
        return view('productos.edit')->with(compact('productos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productos=Producto::all();
        $productos=Producto::findOrFail($id);
        $productos  ->codigo            =$request->get('codigo');
        $productos  ->descripcion       =$request->get('descripcion');
        $productos  ->precio            =$request->get('precio');
        $productos  ->unidad            =$request->get('unidad');
        $productos  ->status            =$request->get('status');

            $request->validate([
                'codigo'=>['required'],
                'descripcion'=>['required'],
                'precio'=>['required'],
                'unidad'=>['required'],
                'status'=>['required'],
            ]);

            $productos->save();
            return redirect()->route('productos.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productos=Producto::findOrFail($id);
        $productos->delete();
        return redirect()->route('productos.index');

    }
}
