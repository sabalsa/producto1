<?php

namespace App\Http\Controllers;

use App\User;
use App\Producto;
use App\Movimiento;
// use App\Http\Controllers\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\SessionGuard;
// use Illuminate\Support\DB;
use Illuminate\Support\Facades\DB;

class MovimientoController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos=Producto::all();
        $movimientos=Movimiento::all()->sortByDesc('id');;
        return view('movimientos.index')->with(compact('movimientos'));

    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {      
        //Esta parte es funcional para crear
        // $productos=Producto::all();
        // $usuario=Auth::user()->name;
        // $movimientos=Movimiento::all();
        // return view('movimientos.create')->with(compact('movimientos','productos', 'usuario'));
    
        // //Se usara para pruebas de una consulta a la base de datos

        $productos=Producto::all()->pluck('codigo');
        $usuario=Auth::user()->name;
        $movimientos=Movimiento::all()->pluck('folio');
        

        // dd($productos, $movimientos);
        
        return view('movimientos.create')->with(compact('movimientos','productos', 'usuario'));
    
}
        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public  function generadorRandom($length = 6) 
    {
        $characters = '0123456789AEIOU';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function store(Request $request)
    {
        $productos=Producto::all();
        $movimientos=Movimiento::all();   
        $movimientos= request()->except('_token');
        $movimientos['folio']=$this->generadorRandom();
        
            $request->validate([
                'fecha'=>['required'],
                'comentario'=>['required'],
                'tipo'=>['required'],
                'status'=>['required'],
                'usuario'=>['required'],
            ]);

        if($movimientos['tipo']==1){
            //Ingreso de productos
            Movimiento::insert($movimientos);
            return redirect()->route('productos.create'); 
        }else{
            //salida de productos
            Movimiento::insert($movimientos);
            return redirect()->route('productos.index'); 

        }  
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function show(Movimiento $movimiento)
    {   
        // $movimientos=Movimiento::find($movimiento->id);
        
        $productos=$movimiento->productos;
        
        return view('movimientos.prueba')->with(compact('productos'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movimientos=Movimiento::all();
        $movimientos=Movimiento::findOrFail($id);
        return view('movimientos.edit')->with(compact('movimientos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $movimientos=Movimiento::all();
        $movimientos=Movimiento::findOrFail($id);
        $movimientos  ->fecha           =$request->get('fecha');
        $movimientos  ->comentario      =$request->get('comentario');
        $movimientos  ->tipo            =$request->get('tipo');
        $movimientos  ->status          =$request->get('status');
        $movimientos  ->usuario         =$request->get('usuario');

            $request->validate([
                'fecha'=>['required'],
                'comentario'=>['required'],
                'tipo'=>['required'],
                'status'=>['required'],
                'usuario'=>['required'],
            ]);

        $movimientos->save();
        return redirect()->route('movimientos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movimiento  $movimiento
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movimientos=Movimiento::findOrFail($id);
        $movimientos->delete();
        return redirect()->route('movimientos.index');
    }
}
