<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = [
        'codigo', 'descripcion', 'precio','unidad','status','folio'
    ];

public function movimiento(){

    return $this->belongsTo('App\Movimiento');

}
}
