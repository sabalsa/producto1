<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{
    protected $table = 'movimientos';
        protected $fillable = ['folio','fecha', 'comentario', 'tipo', 'status', 'usuario' ];


    public function productos(){
        
        return $this->hasMany('App\Producto', 'folio', 'folio');

    }
    }
