<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovimientoDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movimiento_detalles', function (Blueprint $table) {
            $table->id();
            $table->string('movimientos_id')->nullable();
            $table->string('productos_id')->nullable();
            $table->integer('cantidad')->default(0);
            $table->string('movimientos_tipo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movimiento_detalles');
    }
}
