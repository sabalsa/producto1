<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('codigo')->unique();
            $table->text('descripcion')->nullable(); 
            $table->float('precio');
            $table->string('unidad')->nullable();
            $table->integer('status')->default(0);
            $table->string('folio')->nullable(); //Se agrego este campo para referenciar movimientos
            $table->timestamps();                         //Hara refencia solo a un folio de movimientos
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
