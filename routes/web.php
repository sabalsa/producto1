<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    return view('welcome');
});

Route::resource('/productos', 'ProductoController');

Route::resource('/movimientos', 'MovimientoController');

Route::resource('/movimientosDetalles', 'MovimientoDetallesController');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


