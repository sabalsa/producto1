@extends('layouts.app')

@section('content')

<div class="col">
    <a class="btn btn-success" href="/movimientos/create"> Regresar</a>
</div>

{{ Form::open(['url'=>'/productos', 'method'=>'post', 'files'=>True, 'target'=>'volver']) }}
    
        {{ Form::label('codigo','Codigo: ') }}
        {{ Form::text('codigo', null, ['class'=>'form-control']) }}

        {{ Form::label('descripcion','Descripcion: ') }}
        {{ Form::text('descripcion', null, ['class'=>'form-control']) }}

        {{ Form::label('precio','Precio: ') }}
        {{-- {{ Form::text('precio', null,['class'=>'form-control','preciN']) }} --}}
        <input type="number" step="0.01" class="form-control" id="precio" name="precio" Vpattern="^\d+(?:\.\d{1,2})?$">


        {{ Form::label('Unidad','Unidad: ', ) }}
        {{ Form::select('unidad',['Tipo' => ['Pza', 'Kilo','Metro']]) }}

        {{ Form::label('status','Estado: ') }}
        {{ Form::number('status', null) }}

        {{ Form::label('folio  ',' Folio: ',) }}
        {{ Form::text('folio', $movimientos->last(), ['class'=>'form-control'] ) }}

        <div class="row">
            <div class="row" style="margin: 10px">
                <button class="btn btn-success float-right">Subir Producto</button>
            </div>
            <div class="col" style="margin: 20px float-right" >
                <a class="btn btn-primary float-right" href="/productos">Guardar Movimiento</a>
            </div>
        </div>

        <div class="alert alert-danger" >
            @foreach($errors->all() as $error)
            <li><span class="invalid-feedback d-block" role="alert"></span>
                <strong>{{ $error }}</strong>
            </li>  
            @endforeach
        </div>
        {{-- Javascript --}}
        {{ Form::close() }}
<table class="table table-hover" style="width: 1100px; margin: 0 auto;">
    <thead>       
        <tr>
            <th>Codigo</th>
            <th>Descripcion</th>
            <th>Precio</th>
            <th>Unidad</th>
            <th>Status</th>
            <th>FolioMovimiento</th>
        </tr>
    </thead>
    @foreach($productos as $produc) 
        <tbody>
            <tr>
                <td>{{ $produc->codigo }}</td>
                <td>{{ $produc->descripcion }}</td>
                <td> ${{ $produc->precio }}</td>
                <td>{{ $produc->unidad }}</td>
                <td>{{ $produc->status }}</td>
                <td>{{ $produc->folio }}</td>
            </tr>
        </tbody>
    @endforeach  
</table>
@endsection