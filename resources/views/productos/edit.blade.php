@extends('layouts.app')

@section('content')
<div class="col">
    <a class="btn btn-success" href="/productos"> Regresar</a>
</div>

{{ Form::open(['route'=>['productos.update', $productos->id],'method'=>'put']) }}    
    
    {{-- {{ Form::label('movimiento_id', 'Folio') }}
    {{ Form::text('folio', $productos->folio) }} --}}
    
    {{ Form::label('codigo','Codigo: ') }}
    {{ Form::text('codigo', $productos->codigo,['class'=>'form-control']) }}

    {{ Form::label('descripcion','Descripcion: ') }}
    {{ Form::text('descripcion', $productos->descripcion,['class'=>'form-control'] )}}

    {{ Form::label('precio','Precio: ') }}
    {{ Form::text('precio', $productos->precio,['class'=>'form-control']),['class'=>'form-control'] }}

    {{ Form::label('Unidad','Unidad: ') }}
    {{ Form::select('unidad',['Almacen' => ['pza', 'Kilo', 'Metro']], $productos->unidad),['class'=>'form-control'] }}

    {{ Form::label('status','Estado: ') }}
    {{ Form::number('status', $productos->status),['class'=>'form-control'] }}

    <button class="btn btn-success">Actualizar</button>

{{ Form::close() }}
@endsection
