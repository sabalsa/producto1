@extends('layouts.app')

@section('content')
{{-- Aqui comienza el modal --}}
<body >
    <div>
    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Agregar Productos
  </button> --}}
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Productos</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body" >
            {{-- @include('productos.create') --}}
            {{ Form::open(['url'=>'/productos', 'method'=>'post',  'files'=>True]) }}
    
                {{ Form::label('codigo','Codigo: ') }}
                {{ Form::text('codigo', null) }}

                {{ Form::label('descripcion','Descripcion: ') }}
                {{ Form::text('descripcion', null) }}

                {{ Form::label('precio','Precio: ') }}
                {{ Form::text('precio', null) }}

                {{ Form::label('Unidad','Unidad: ') }}
                {{ Form::select('unidad',['Tipo' => ['Pza', 'Kilo','Metro']]) }}

                {{ Form::label('status','Estado: ') }}
                {{ Form::number('status', null) }}

                {{ Form::label('folio  ',' Folio: ') }}
                {{ Form::select('folio', $movimientos ) }}  

                    <button class="btn btn-success">Crear</button>
            {{ Form::close() }}
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>    
        </div>
        </div>
      </div>
    </div>
  </div>
    </div>
    {{-- Aqui termina el modal --}}
    </div>
    <table  class="table table-hover" style="width: 1100px; margin: 0 auto;">
        <thead>       
            <tr>
                <th></th>
                <th></th>
                <th>Codigo</th>
                <th>Descripcion</th>
                <th>Precio</th>
                <th>Unidad</th>
                <th>Status</th>
                <th>FolioMovimiento</th>
            </tr>
        </thead>
        @foreach($productos as $produc) 
            <tbody>
                <tr>
                    <td><a href="{{ route('productos.edit', $produc->id )}}" class="btn btn-success">Editar</a></td>
                    <td>
                        {{ Form::open(['method'=>'DELETE', 'route'=>['productos.destroy', $produc->id]])}}
                            <button class="btn btn-danger">Eliminar</button>
                        {{ Form::close() }}
                    </td>
                    <td>{{ $produc->codigo }}</td>
                    <td>{{ $produc->descripcion }}</td>
                    <td> ${{ $produc->precio }}</td>
                    <td>{{ $produc->unidad }}</td>
                    <td>{{ $produc->status }}</td>
                    <td>{{ $produc->folio }}</td>
                    {{-- <td>{{ $movimientos->movimientos_id }}</td> --}}
                </tr>
            </tbody>
        @endforeach
        
    </table>
</body>
@endsection