@extends('layouts.app')

@section('content')

<h6>rVMqfS2aJGUth29</h6>

<div class="col">
<a class="btn btn-sussec" href="/movimientos">regresar</span></span></a>
</div>

<div>
{{ Form::open(['url'=>'/movimientos', 'method'=>'post', 'files'=>True, 'novalidate']) }}
    
    {{ Form::label('fecha','Fecha ') }}
    {{ Form::date('fecha', \Carbon\Carbon::now(), ['class'=>'form-control']) }}

    {{ Form::label('comentario','Comentarios: ') }}
    {{ Form::text('comentario', null, ['class'=>'form-control']) }}

    {{ Form::label('tipo','Tipo: ') }}
    {{ Form::select('tipo',[
        'Almacen' => ['Salida', 'Entrada']], ['class'=>'form-control']) }}
 
    {{ Form::label('status','Estado: ', ['class'=>'form-control']) }}
        <p>0=Baja 1=Activa 2=Aplicada</p>
    {{ Form::select('status',['Estado' => ['0', '1','2']], ['class'=>'form-control']) }}

    {{ Form::label('usuario','Usuario: ') }}
    {{ Form::text('usuario', $usuario, ['class'=>'form-control'] ) }}

      <button class="btn btn-success">Crear Movimiento</button>

{{ Form::close() }}

@endsection
{{-- Add modal --}}
<!-- Button trigger modal -->
    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Agregar Productos
      </button>
      
      
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Productos</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" >
            {{-- @include('productos.create') --}}
            {{-- {{ Form::open(['url'=>'/productos', 'method'=>'post',  'files'=>True]) }}
    
                {{ Form::label('codigo','Codigo: ') }}
                {{ Form::text('codigo', null) }}

                {{ Form::label('descripcion','Descripcion: ') }}
                {{ Form::text('descripcion', null) }}

                {{ Form::label('precio','Precio: ') }}
                {{ Form::text('precio', null) }}

                {{ Form::label('Unidad','Unidad: ') }}
                {{ Form::select('unidad',['Tipo' => ['Pza', 'Kilo','Metro']]) }}

                {{ Form::label('status','Estado: ') }}
                {{ Form::number('status', null) }}
                
                <button class="btn btn-success" >Guardar</button>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        {{-- <button type="button" class="btn btn-primary">Guardar</button> --}}
                    {{-- </div> --}}

            {{-- {{ Form::close() }} --}}
        {{-- </div> --}}
        
      {{-- </div> --}}
    {{-- </div>  --}}
  {{-- </div> --}}
