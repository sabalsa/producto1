@extends('layouts.app')

@section('content')
    <div class="">
        <a class="btn btn-primary" href='/movimientos/create'>Añadir Movimiento</a>
    </div>
<div style="margin: 20px">
    <table class="table table-hover" style="width: 1100px; margin: 0 auto;">
        <thead>    
            <tr>
                <th></th>
                <th></th>
                <th>Folio</th>
                <th>Fecha</th>
                <th>Descripcion</th>
                <th>Tipo</th>
                <th>Estado</th>
                <th>Usuario</th>
            </tr>
        </thead>
        @foreach($movimientos as $movi) 
            <tbody>
                <tr>
                    <td><a href="{{ route('movimientos.edit', $movi->id )}}" class="btn btn-success">Editar</a></td>
                    <td>
                        {{ Form::open(['method'=>'DELETE', 'route'=>['movimientos.destroy', $movi->id]])}}
                            <button class="btn btn-danger">Eliminar</button>
                        {{ Form::close() }}
                    </td>
                    <td>{{ $movi->folio }}</td>
                    <td>{{ $movi->fecha }}</td>
                    <td>{{ $movi->comentario }}</td>
                    <td>{{ $movi->tipo }}</td>
                    <td>{{ $movi->status }}</td>
                    <td>{{ $movi->usuario }}</td>
                    {{-- <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{ $movi->id }}">
                        Ver Detalles
                        </button>
                    </td>   --}}
                    <td>
                      <a href="{{  route('movimientos.show', $movi->id) }}" class="btn btn-success">Ver</a>
                    </td>
                </tr>
            </tbody>
            <div class="modal fade" id="exampleModal{{ $movi->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ $movi->fecha }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                        {{ $movi->folio }}
                        {{ $movi->comentario }}
                        {{ $movi->tipo }}
                  </div>
                  <div class="modal-footer">
                  </div>
                </div>
              </div>
            </div>
        @endforeach                     
    </table>
</div>
@endsection