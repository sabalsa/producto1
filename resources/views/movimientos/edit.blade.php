
@extends('layouts.app')

@section('content')<h1> Estas en editar</h1>
<div class="col">
    <a class="btn" href="/movimientos"> Regresar</a>
</div>

{{ Form::open(['route'=>['movimientos.update', $movimientos->id],'method'=>'put']) }}    
    
    {{-- {{ Form::label('folio','Folio') }}
    {{ Form::text('movimientos_id', $movimientos->movimientos_id) }} --}}

    {{ Form::label('fecha','Fecha ') }}
    {{ Form::date('fecha', $movimientos->fecha,['class'=>'form-control']) }}

    {{ Form::label('comentario','Comentarios: ') }}
    {{ Form::text('comentario', $movimientos->comentario, ['class'=>'form-control']) }}

    {{ Form::label('tipo','Tipo: ') }}
    {{ Form::select('tipo',[
        'Almacen' => ['Salida', 'Entrada']], $movimientos->tipo) }}

    {{ Form::label('status','Estado: ') }}
        <p>0=Baja 1=Activa 2=Aplicada</p>
    {{ Form::select('status',['Estado' => ['0', '1','2']], $movimientos->status) }}

    {{ Form::label('usuario','Usuario: ') }}
    {{ Form::text('usuario', $movimientos->usuario, ['class'=>'form-control']) }}

        <button class="btn btn-success">Guardar</button>

{{ Form::close() }}
@endsection