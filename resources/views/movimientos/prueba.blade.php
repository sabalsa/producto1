@extends('layouts.app')

@section('content')
<h1>Detalles</h1>

@foreach($productos as $producto)

        <table class="table table-light">
            <tbody>
                <tr>
                    <td>{{ $producto->codigo }}</td>
                    <td>{{ $producto->descripcion }}</td>
                    <td>{{ $producto->precio }}</td>
                </tr>
            </tbody>
        </table>
@endforeach

@endsection



